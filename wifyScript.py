import os
import time
import subprocess

#Global Variables
global fileLocation

bol = 0

#Variables
#fileLocation = '/Users/Brian/PycharmProjects/Learning/command.txt'
fileLocation = os.listdir('~')



def Ping():
    subprocess.Popen("ping -t 5 www.google.com | cat >> ~/command.txt", shell=True)


def listDirectory():
    process = subprocess.Popen(['ls'], shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    a = process.communicate()
    return [a]


def fileOpen():
    fo = open(fileLocation, "r+")
    buffer = fo.read(1000)
    fo.close()
    return [buffer]

def printFunction(ourPrint):
    print(": ", ourPrint)

def oneFunction():
    global bol                              #global Boolean Variable
    Ping()
    #print('waiting 8 sec')
    time.sleep(8)           #Sleep 8 seconds
    #ping = fileOpen()
    #printFunction(ping)
    if os.stat(fileLocation).st_size == 0:  # If file is Empty Ping did not find Host, meaning that
        bol = 1                             # there is no internet
    else:
        bol = 0
    os.remove(fileLocation)

def wifiSwitch(bol):
    if bol == 0:
        print("Airport ON")
    else:
        print("Airport OFF")


def main():
    global  bol                             #global Boolean Variable

    oneFunction()
     #wifiSwitch(bol)
    while True:
     if bol == 0:
          time.sleep(1200)
          oneFunction()
     elif bol != 0:
         os.system('networksetup -setairportpower en0 off')
         time.sleep(600)
         os.system('networksetup -setairportpower en0 on')
         time.sleep(5)
         oneFunction()

if __name__ == "__main__":
    main()
